//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Felipe Marques Ramos on 2/18/22.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
