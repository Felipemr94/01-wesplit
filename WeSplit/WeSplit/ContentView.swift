//
//  ContentView.swift
//  WeSplit
//
//  Created by Felipe Marques Ramos on 2/18/22.
//

import SwiftUI

struct ContentView: View {

    @State private var checkAmount = 0.0
    @State private var numberOfPeopleIndex = 0
    @State private var tipPercentage = 10

    @FocusState private var amountIsFocused: Bool

    let tipPercentages = [10,15,20,25,0]
    var amountWithTip: Double {
        let tipSelection = Double(tipPercentage)
        let tipValue = checkAmount / 100 * tipSelection

        return checkAmount + tipValue
    }

    var totalPerPerson: Double {
        let peopleCount = Double(numberOfPeopleIndex + 2)
        let amountPerPerson = amountWithTip / peopleCount

        return amountPerPerson
    }

    private var useRedText: Bool {
        return tipPercentage == 0
    }

    let currencyFormat: FloatingPointFormatStyle<Double>.Currency = .currency(code: Locale.current.currencyCode ?? "BRL")

    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Amount", value: $checkAmount, format: currencyFormat)
                        .keyboardType(.decimalPad)
                        .focused($amountIsFocused)

                    Picker("Number of people", selection: $numberOfPeopleIndex) {
                        ForEach(2..<100) {
                            Text("\($0) people")
                        }
                    }
                } header: {
                    Text("Amount")
                }
                Section {
                    Picker("Tip", selection: $tipPercentage) {
                        ForEach(tipPercentages, id: \.self) {
                            Text($0,format: .percent)
                                .foregroundColor(useRedText ? .red : .blue)
                        }
                    }
//                    .pickerStyle(.menu)
                } header: {
                    Text("What is your tip?")
                }footer: {
                    Text(amountWithTip, format: currencyFormat)
                }
                
                Section {
                    Text(totalPerPerson, format: currencyFormat)
                } header: {
                    Text("Total per person")
                }
            }
            .navigationTitle("WeSplit")
            .toolbar {
                ToolbarItemGroup(placement: .keyboard) {
                    Spacer()

                    Button("Done") {
                        amountIsFocused = false
                    }
                }
            }
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
